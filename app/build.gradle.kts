import Build_gradle.Variant.PROD
import Build_gradle.Variant.REC

object Variant {
    const val REC = "rec"
    const val PROD = "prod"
}

val ktlint: Configuration by configurations.creating

apply(from = rootProject.file("jacoco.gradle"))

plugins {
    id("com.android.application")
    id("com.google.gms.google-services")
    id("com.google.firebase.crashlytics")
    kotlin("android")
    kotlin("kapt")

    // Create task "Tasks/help/dependencyUpdates" that check all dependencies versions
    // https://github.com/ben-manes/gradle-versions-plugin
    id(Libs.Plugins.GRADLE_VERSIONS) version Versions.Plugins.GRADLE_VERSIONS
}

android {
    signingConfigs {
        create("covid19") {
            storeFile = file("keystores/upload-keystore.jks")
            storePassword = "daly1992"
            keyAlias = "upload"
            keyPassword = "1992daly"
        }
    }
    compileSdk = AndroidConfig.COMPILE_SDK_VERSION
    buildToolsVersion = AndroidConfig.BUILD_TOOLS_VERSION

    defaultConfig {
        applicationId = AndroidConfig.APPLICATION_ID
        minSdk = AndroidConfig.MIN_SDK_VERSION
        targetSdk = AndroidConfig.TARGET_SDK_VERSION
        val version = 2022043002 // YYYYMMDD<versionOfTheDay>
        versionCode = version
        versionName = "rec-$version"

        testInstrumentationRunner = AndroidConfig.ANDROID_TEST_INSTRUMENTATION

        // Fix some error with VectorDrawable compatibility
        // Error : Can't process attribute android:strokeColor="@color/coral"
        // Error : File was preprocessed as vector drawable android:filltype support was added in Android 7.0 (API level 24)
        vectorDrawables.useSupportLibrary = true
    }

    buildFeatures.dataBinding = true

    sourceSets {
        getByName("androidTest").java.srcDirs("src/androidTest/kotlin")
        getByName("main").java.srcDirs("src/main/kotlin")
        getByName("test").java.srcDirs("src/test/kotlin")
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
            signingConfig = signingConfigs.getByName("covid19")
        }

        getByName("debug") {
            signingConfig = signingConfigs.getByName("covid19")
        }
    }

    flavorDimensions("environment")
    productFlavors {
        create(REC) {
            isDefault = true
            dimension = "environment"
            applicationIdSuffix = ".rec"
        }
        create(PROD) {
            dimension = "environment"
            applicationIdSuffix = ""
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }

    lint {
        // To allow making high version code value
        disable("HighAppVersionCode")

        isAbortOnError = true
        isWarningsAsErrors = true
        isCheckDependencies = true

        /** Muted IssueId of lint warnings thrown by Android */
        this.informational(
            // When dependencies aren't the latest
            "GradleDependency",
            // When path in vectorDrawable are too long
            "VectorPath",
            // Single FrameLayout in xml file
            "MergeRootFrame",
            // src/res contain unused content
            "UnusedResources",
            // When attribute allow_backup in <application> xml tag is set
            // Warning of xml listing is needed
            "AllowBackup",
            // Context in ViewModel.
            "StaticFieldLeak",
            // Allow untrusted certificates for https
            "TrustAllX509TrustManager",
            // Warning for spelling typo
            "Typos",
            // Warning for inconsistencies in array element counts
            "InconsistentArrays"
        )
    }

    // This lint will be done on Quality stage of Gitlab-CI
//    applicationVariants.all {
//        val lintTask = project.tasks.getByName("lint${name.capitalize()}")
//        assembleProvider?.configure {
//            dependsOn(lintTask)
//        }
//    }

    kotlinOptions {
        jvmTarget = "11"
        allWarningsAsErrors = true
        freeCompilerArgs = freeCompilerArgs + listOf("-Xopt-in=kotlin.RequiresOptIn")
    }

    externalNativeBuild {
        ndkBuild {
            path = file("src/main/jni/Android.mk")
        }
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
    implementation(Dependencies.appLibraries)
    implementationPlatform(Dependencies.platformLibraries)
    kapt(Dependencies.dataBindingLibraries)
    testImplementation(Dependencies.testLibraries)
    ktlint(Libs.Lint.KTLINT)
}

val outputDir = "${project.buildDir}/reports/ktlint/"
val inputFiles = project.fileTree(mapOf("dir" to "src", "include" to "**/*.kt"))

/**
 * Task that check code style defined in ".editorconfig" file
 */
tasks.register("ktlintCheck", JavaExec::class.java) {
    inputs.files(inputFiles)
    outputs.dir(outputDir)

    group = "verification"
    description = "Check Kotlin code style."
    classpath = ktlint
    main = "com.pinterest.ktlint.Main"
    args = listOf("--android", "src/**/*.kt")
}

/**
 * Task that reformat code to follow code style defined in ".editorconfig" file
 */
tasks.register("ktlintFormat", JavaExec::class.java) {
    inputs.files(inputFiles)
    outputs.dir(outputDir)

    group = "formatting"
    description = "Fix Kotlin code style deviations."
    classpath = ktlint
    main = "com.pinterest.ktlint.Main"
    args = listOf("--android", "-F", "src/**/*.kt")
}

tasks.withType<Test> {
    useJUnitPlatform()
    testLogging {
        events("passed", "skipped", "failed")
    }
}