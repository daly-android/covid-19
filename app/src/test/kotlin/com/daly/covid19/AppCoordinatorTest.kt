package com.daly.covid19

import com.daly.covid19.common.core.interfaces.IRouter
import com.daly.covid19.overview.presentation.views.OverviewFragment
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.justRun
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExperimentalCoroutinesApi
@ExtendWith(InstantExecutorExtension::class, MockKExtension::class)
class AppCoordinatorTest {

    @MockK
    private lateinit var router: IRouter

    private lateinit var coordinator: AppCoordinator

    @BeforeEach
    fun setup() {
        coordinator = AppCoordinator(router)
    }

    @Test
    fun testGotToOverview() {
        // given
        justRun { router.show(OverviewFragment::class.java) }

        // when
        coordinator.gotToOverview()

        // then
        verify(exactly = 1) {
            router.show(OverviewFragment::class.java)
        }
    }
}