package com.daly.covid19

import com.daly.covid19.common.monitoring.Monitor
import com.daly.covid19.common.test.TestMonitor
import io.mockk.every
import io.mockk.mockkObject
import io.mockk.unmockkObject

/**
 * Mock QuickMonitorLogging with TestMonitor implementation
 * Used on unit tests
 * Example :
 *    @BeforeEach
 *    fun before() {
 *        mockMonitorLogging()
 *        ...
 *    }
 */
fun mockMonitorLogging() {
    val monitor = TestMonitor()
    mockkObject(Monitor)
    every { Monitor.logD(any()) } answers { monitor.logD(this.invocation.args[0] as String) }
    every { Monitor.logV(any()) } answers { monitor.logV(this.invocation.args[0] as String) }
    every { Monitor.logW(any()) } answers { monitor.logW(this.invocation.args[0] as String) }
    every { Monitor.logE(any(), any()) } answers { monitor.logE(this.invocation.args[0] as String, this.invocation.args[1] as Throwable?) }
}

/**
 * Unmock QuickMonitorLogging
 * Used on unit tests
 * Example :
 *    @AfterEach
 *    fun after() {
 *        unmockMonitorLogging()
 *        ...
 *    }
 */
fun unmockMonitorLogging() {
    unmockkObject(Monitor)
}