package com.daly.covid19.overview.data.repositories

import com.daly.covid19.DummyCall
import com.daly.covid19.InstantExecutorExtension
import com.daly.covid19.overview.data.api.CovidApi
import com.daly.covid19.overview.data.mappers.toDomainModel
import com.daly.covid19.overview.data.models.LatestTotals
import com.daly.covid19.overview.domain.repositories.IOverviewRepository
import com.daly.covid19.overview.domain.repositories.RepositoryError
import com.daly.covid19.overview.domain.repositories.RepositoryResult
import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import io.mockk.verify
import kotlin.time.ExperimentalTime
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import retrofit2.Call

@OptIn(ExperimentalCoroutinesApi::class, ExperimentalTime::class)
@ExtendWith(InstantExecutorExtension::class, MockKExtension::class)
internal class OverviewRepositoryUnitTest {

    companion object {
        private val LATEST_TOTALS = LatestTotals(
            123456789L,
            123456789L,
            123456L,
            567890L,
            "2021-04-28T17:57:49+02:00",
            "2021-04-28T18:00:04+02:00"
        )
        private val LIST_LATEST_TOTALS = listOf(LATEST_TOTALS)
    }

    private val covidApi = mockk<CovidApi>()
    private lateinit var repository: IOverviewRepository

    @BeforeEach
    fun before() {
        repository = OverviewRepository(covidApi)
    }

    @Test
    fun testFetchLatestTotals() = runBlockingTest {
        // given
        every { covidApi.getLatestTotalsUsingGET() } answers { DummyCall.success { LIST_LATEST_TOTALS } }

        // when
        val returnData = repository.fetchLatestTotals().single()

        // then
        assertEquals(RepositoryResult.Success(LATEST_TOTALS.toDomainModel()), returnData)

        verify(exactly = 1) {
            covidApi.getLatestTotalsUsingGET()
        }
    }

    @Test
    fun testFailureFetchLatestTotals() = runBlocking {
        // given
        val callFailure: Call<List<LatestTotals>> = DummyCall.failure(401)
        every { covidApi.getLatestTotalsUsingGET() } answers { callFailure }

        // when
        val returnData = repository.fetchLatestTotals().single()

        // then
        assertEquals(RepositoryResult.Failure(RepositoryError.NotAuthorized), returnData)

        verify(exactly = 1) {
            covidApi.getLatestTotalsUsingGET()
        }
    }

    @Test
    fun testErrorFetchLatestTotals() = runBlocking {
        // given
        val callError: Call<List<LatestTotals>> = DummyCall.error(Throwable("Error"))
        every { covidApi.getLatestTotalsUsingGET() } answers { callError }

        // when
        val returnData = repository.fetchLatestTotals().single()

        // then
        assertEquals(RepositoryResult.Failure(RepositoryError.Internal), returnData)

        verify(exactly = 1) {
            covidApi.getLatestTotalsUsingGET()
        }
    }
}