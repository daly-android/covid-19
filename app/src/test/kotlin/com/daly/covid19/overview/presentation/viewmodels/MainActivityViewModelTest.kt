package com.daly.covid19.overview.presentation.viewmodels

import android.content.Context
import com.daly.covid19.AppCoordinator
import com.daly.covid19.InstantExecutorExtension
import com.daly.covid19.common.utils.NetworkUtils
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.justRun
import io.mockk.mockkObject
import io.mockk.unmockkObject
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@OptIn(ExperimentalCoroutinesApi::class)
@ExtendWith(InstantExecutorExtension::class, MockKExtension::class)
class MainActivityViewModelTest {

    @MockK
    private lateinit var coordinator: AppCoordinator

    @MockK
    private lateinit var context: Context

    private lateinit var viewModel: MainActivityViewModel

    @BeforeEach
    fun setup() {
        mockkObject(NetworkUtils)
        justRun { coordinator.showNetworkUnavailableAlertDialog(context) }

        viewModel = MainActivityViewModel(coordinator)
    }

    @AfterEach
    fun after() {
        unmockkObject(NetworkUtils)
    }

    @Test
    fun testLoadNetworkAvailable() = runBlocking {
        // given
        every { NetworkUtils.isNetworkAvailable(context) } returns true

        justRun { coordinator.gotToOverview() }

        // when
        viewModel.load(context)

        // then
        verify(exactly = 1) {
            coordinator.gotToOverview()
        }
        verify(exactly = 0) {
            coordinator.showNetworkUnavailableAlertDialog(context)
        }

        assertEquals(true, viewModel.isReady)
    }

    @Test
    fun testLoadNetworkNotAvailable() = runBlocking {
        // given
        every { NetworkUtils.isNetworkAvailable(context) } returns false

        // when
        viewModel.load(context)

        // then
        verify(exactly = 1) {
            coordinator.showNetworkUnavailableAlertDialog(context)
        }
        verify(exactly = 0) {
            coordinator.gotToOverview()
        }

        assertEquals(true, viewModel.isReady)
    }
}