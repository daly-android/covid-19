package com.daly.covid19.overview.presentation.mappers

import com.daly.covid19.InstantExecutorExtension
import com.daly.covid19.common.core.interfaces.IResourcesRepository
import com.daly.covid19.common.utils.CategoryUtils
import com.daly.covid19.databuilder.DataBuilderCategoryInfo
import com.daly.covid19.databuilder.DataBuilderLatestTotalsModel
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.junit5.MockKExtension
import io.mockk.mockkStatic
import io.mockk.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExperimentalCoroutinesApi
@ExtendWith(InstantExecutorExtension::class, MockKExtension::class)
class CategoryLatestTotalsViewMapperUnitTest {

    @RelaxedMockK
    private lateinit var resourcesRepository: IResourcesRepository

    @BeforeEach
    fun setUp() {
        mockkStatic("com.daly.covid19.overview.presentation.mappers.CategoryLatestTotalsViewMapperKt")
    }

    @Test
    fun testToViewData() {
        // given
        val latestTotalsModel = DataBuilderLatestTotalsModel.create()

        // when
        val result = latestTotalsModel.toViewData(resourcesRepository)

        // then
        assertEquals(CategoryUtils.insertSpacesBetweenNumbers(DataBuilderCategoryInfo.CONFIRMED.toString()), result.first[0].categoryValue.value)
        assertEquals(CategoryUtils.insertSpacesBetweenNumbers(DataBuilderCategoryInfo.RECOVERED.toString()), result.first[1].categoryValue.value)
        assertEquals(CategoryUtils.insertSpacesBetweenNumbers(DataBuilderCategoryInfo.CRITICAL.toString()), result.first[2].categoryValue.value)
        assertEquals(CategoryUtils.insertSpacesBetweenNumbers(DataBuilderCategoryInfo.DEATHS.toString()), result.first[3].categoryValue.value)
        assertEquals(latestTotalsModel.lastChange, result.second.lastChangeText.value)

        verify {
            setupCategoryLatestTotalsViewData(any(), resourcesRepository)
        }
    }
}