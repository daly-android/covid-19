package com.daly.covid19.common.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import com.daly.covid19.InstantExecutorExtension
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

@OptIn(ExperimentalCoroutinesApi::class)
@ExtendWith(InstantExecutorExtension::class, MockKExtension::class)
class NetworkUtilsTest {

    @MockK
    private lateinit var context: Context

    @MockK
    private lateinit var connectivityManager: ConnectivityManager

    @MockK
    private lateinit var networkCapabilities: NetworkCapabilities

    @MockK
    private lateinit var network: Network

    @BeforeEach
    fun setup() {
        every { context.getSystemService(any()) } returns connectivityManager
        every { connectivityManager.getNetworkCapabilities(any()) } returns networkCapabilities
        every { connectivityManager.activeNetwork } returns network
        every { networkCapabilities.hasCapability(any()) } returns true
    }

    @ParameterizedTest
    @ValueSource(booleans = [true, false])
    fun testIsNetworkAvailable(expected: Boolean) {
        // given
        every { networkCapabilities.hasCapability(any()) } returns expected

        // when
        val result = NetworkUtils.isNetworkAvailable(context)

        // then
        assertEquals(expected, result)
    }

    @Test
    fun testIsNetworkAvailableConnectivityManagerNull() {
        // given
        every { context.getSystemService(any()) } returns null

        // when
        val result = NetworkUtils.isNetworkAvailable(context)

        // then
        assertEquals(false, result)
    }
}