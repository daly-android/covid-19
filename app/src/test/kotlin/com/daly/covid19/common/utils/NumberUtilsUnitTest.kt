package com.daly.covid19.common.utils

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class NumberUtilsUnitTest {

    companion object {
        private const val DOUBLE_VALUE_WITHOUT_ROUNDING = 5.4321
        private const val DOUBLE_VALUE_WITH_ROUNDING = 5.1992

        private const val EXPECTED_STRING_VALUE_ONE_FRACTION_DIGIT = "5.4"
        private const val EXPECTED_STRING_VALUE_TWO_FRACTION_DIGITS = "5.43"
        private const val EXPECTED_STRING_VALUE_TWO_FRACTION_DIGITS_WITH_ROUNDING = "5.20"
    }

    @Test
    fun testDefaultFractionDigits() {
        // when
        val value = NumberUtils.formatFixedFractionDigits(DOUBLE_VALUE_WITHOUT_ROUNDING)

        // then
        assertEquals(EXPECTED_STRING_VALUE_ONE_FRACTION_DIGIT, value)
    }

    @Test
    fun testFormatFixedFractionDigitsWithoutRounding() {
        // when
        val value = NumberUtils.formatFixedFractionDigits(DOUBLE_VALUE_WITHOUT_ROUNDING, 2)

        // then
        assertEquals(EXPECTED_STRING_VALUE_TWO_FRACTION_DIGITS, value)
    }

    @Test
    fun testFormatFixedFractionDigitWithRounding() {
        // when
        val value = NumberUtils.formatFixedFractionDigits(DOUBLE_VALUE_WITH_ROUNDING, 2)

        // then
        assertEquals(EXPECTED_STRING_VALUE_TWO_FRACTION_DIGITS_WITH_ROUNDING, value)
    }
}