package com.daly.covid19.common.extensions

import android.content.Context
import android.widget.Toast
import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(MockKExtension::class)
internal class ContextExtensionsUnitTest {

    companion object {
        private const val TEXT = "My Toast text"
        private const val RES_ID = 0
    }
    private val context = mockk<Context>()
    private val toast = mockk<Toast>(relaxed = true)

    @BeforeEach
    private fun before() {
        mockkStatic(Toast::class)
        every {
            Toast.makeText(context, any<String>(), any()) // any text as CharSequence, any duration
        } answers {
            toast
        }
        every {
            Toast.makeText(context, any<Int>(), any()) // any resId as Int, any duration
        } answers {
            toast
        }
    }

    @Test
    fun longToastResId() {
        // when
        context.longToast(RES_ID)

        // then
        verify(exactly = 1) {
            Toast.makeText(context, RES_ID, Toast.LENGTH_LONG)
            toast.show()
        }
    }

    @Test
    fun longToastCharSequence() {
        // when
        context.longToast(TEXT)

        // then
        verify(exactly = 1) {
            Toast.makeText(context, TEXT, Toast.LENGTH_LONG)
            toast.show()
        }
    }

    @Test
    fun shortToastResId() {
        // when
        context.shortToast(RES_ID)

        // then
        verify(exactly = 1) {
            Toast.makeText(context, RES_ID, Toast.LENGTH_SHORT)
            toast.show()
        }
    }

    @Test
    fun shortToastCharSequence() {
        // when
        context.shortToast(TEXT)

        // then
        verify(exactly = 1) {
            Toast.makeText(context, TEXT, Toast.LENGTH_SHORT)
            toast.show()
        }
    }
}