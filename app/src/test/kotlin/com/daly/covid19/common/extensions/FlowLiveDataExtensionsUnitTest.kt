package com.daly.covid19.common.extensions

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.daly.covid19.InstantExecutorExtension
import kotlin.time.ExperimentalTime
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith

@InternalCoroutinesApi
@ExperimentalTime
@ExperimentalCoroutinesApi
@ExtendWith(InstantExecutorExtension::class)
@DisplayName("LiveData extensions")
internal class FlowLiveDataExtensionsUnitTest {

    companion object {
        const val DATA1 = "DATA1"
        const val DATA2 = "DATA2"
        const val DATA3 = "DATA3"
    }

    private val _liveData = MutableLiveData<String>()
    private val liveData: LiveData<String> = _liveData

    @Test
    @DisplayName("Single LiveData update")
    fun onSingleUpdate() = runBlockingTest {
        val liveFlow = liveData.asFlow()
        _liveData.value = DATA1
        Assertions.assertEquals(DATA1, liveFlow.single())
    }

    @Test
    @DisplayName("Multiple LiveData update")
    fun onMultipleUpdate() = runBlockingTest {
        val liveFlow = liveData.asFlow(3)
        _liveData.value = DATA1
        _liveData.value = DATA2
        _liveData.value = DATA3

        Assertions.assertEquals(DATA1, liveFlow.first())
        Assertions.assertEquals(DATA2, liveFlow.first())
        Assertions.assertEquals(DATA3, liveFlow.first())
    }
}