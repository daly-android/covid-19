package com.daly.covid19

import com.daly.covid19.common.core.implementations.Router
import com.daly.covid19.common.core.interfaces.IKoinModule
import com.daly.covid19.common.core.interfaces.IRouter
import com.daly.covid19.common.monitoring.MonitorImpl
import com.daly.covid19.overview.data.api.CovidApi
import com.daly.covid19.overview.data.common.interceptors.AuthenticationInterceptor
import com.daly.covid19.overview.data.common.utils.RetrofitUtils
import com.daly.covid19.overview.data.repositories.OverviewRepository
import com.daly.covid19.overview.domain.repositories.IOverviewRepository
import com.daly.covid19.overview.presentation.viewmodels.MainActivityViewModel
import com.daly.covid19.overview.presentation.viewmodels.OverviewFragmentViewModel
import com.daly.covid19.overview.presentation.views.OverviewFragment
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.androidx.fragment.dsl.fragment
import org.koin.core.module.Module
import org.koin.dsl.module

object MainKoinModule : IKoinModule {

    private val network = module {
        single {
            Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
        }

        // Interceptor
        single { AuthenticationInterceptor() }

        single { RetrofitUtils.provideAuthenticateRetrofit<CovidApi>(get(), get(), get()) }
    }

    private val overview = module {
        single<IOverviewRepository> { OverviewRepository(get()) }
        fragment { OverviewFragment() } // Can be removed because until now we don't inject fragments using Koin but we use IRouter to instantiate them

        viewModel {
            OverviewFragmentViewModel(get(), get())
        }

        viewModel {
            MainActivityViewModel(get())
        }
    }

    private val coreApp = module {
        single { MonitorImpl() }
        single { AppCoordinator(get()) }
        single<IRouter> { Router() }
    }

    override val modules: List<Module> = listOf(network, overview, coreApp)
}