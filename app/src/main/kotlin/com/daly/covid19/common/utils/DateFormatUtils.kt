package com.daly.covid19.common.utils

import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale
import java.util.concurrent.TimeUnit

/**
 * Utility to format dates.
 *
 * It allows you to make several transformations :
 * - String -> Date
 * - Date -> String
 * - Long -> String
 */
object DateFormatUtils {

    /**
     * Enum containing all formats that came from API
     */
    enum class FormatApi(val pattern: String) {
        FULL("yyyy-MM-dd'T'HH:mm:ss'Z'"),
        FULL_FR("HH:mm:ss dd/MM/yyyy"),
        FULL_ISO_8601("yyyy-MM-dd HH:mm:ss Z"),
        FULL_ISO_8601_TIME_ZONE("yyyy-MM-dd'T'HH:mm:ssXXX"),
        REGULAR("yyyy-MM-dd")
    }

    /**
     * Enum containing all formats that we can display
     */
    enum class FormatHumanReadable(val pattern: String) {
        LETTERS_FULL("EEEE dd MMMM yyyy"), // ex: lundi 5 octobre 2020
        LETTERS_WITHOUT_WEEK_DAY("dd MMMM yyyy"), // ex: 5 octobre 2020
        LETTERS_WITHOUT_WEEK_DAY_WITH_ABBREVIATED_MONTH("dd MMM yyyy"), // ex: 5 oct. 2020
        LETTER_WITHOUT_DAY("MMMM yyyy"), // ex : octobre 2020
        LETTERS_ONLY_MONTH("MMMM"), // ex : octobre
        DIGITS("dd/MM/yyyy"), // ex: 05/10/2020
        DIGITS_WITH_LIGHT_DATE("dd/MM/yy"), // ex : 05/10/20
        DIGITS_ONLY_YEAR("yyyy"), // ex : 2020
        DIGITS_WITHOUT_YEAR("dd/MM"), // ex : 05/10
        DIGITS_ONLY_HOUR_MINUTES("HH:mm"), // ex : 07:55
        DIGITS_ONLY_HOUR_MINUTES_SEPARATED_BY_LETTER("HH'h'mm"), // ex : 07h55
        DIGITS_WITHOUT_DAY("MM/yy") // ex: 10/21
    }

    /**
     * Return number of minutes passed from String date and time
     *
     * @param stringDataMillis
     * @return number of minutes
     */
    fun getPassedMinutesFromDateMillis(stringDataMillis: String?): Int? {
        // TODO : Check Time zone ?
        return try {
            TimeUnit.MILLISECONDS.toMinutes(getToday().time - getDateFromString(stringDataMillis, FormatApi.FULL_ISO_8601_TIME_ZONE)!!.time).toInt()
        } catch (e: java.lang.Exception) {
            null
        }
    }

    /**
     * @see [getDateFromString(String?, String)]
     */
    fun getDateFromString(value: String?, format: FormatApi) = getDateFromString(value, format.pattern)

    /**
     * @see [getDateFromString(String?, String)]
     */
    fun getDateFromString(value: String?, format: FormatHumanReadable) = getDateFromString(value, format.pattern)

    /**
     * Parse a given [date] to get a [Date] using the given [format]
     * @param date date to parse
     * @param format format to use to parse
     * @return date, or null
     */
    fun getDateFromString(date: String?, format: String): Date? {
        date ?: return null

        return try {
            SimpleDateFormat(format, Locale.FRANCE).parse(date)
        } catch (e: Exception) {
            null
        }
    }

    /**
     * @see [toDateString(String)]
     */
    fun Date?.toDateString(format: FormatApi) = toDateString(format.pattern)

    /**
     * @see [toDateString(String)]
     */
    fun Date?.toDateString(format: FormatHumanReadable) = toDateString(format.pattern)

    /**
     * Parse date to get [String] representation using the given [format]
     * @param format format to use to convert
     * @return formatted date, or null
     */
    fun Date?.toDateString(format: String): String? {
        this ?: return null
        return try {
            SimpleDateFormat(format, Locale.FRANCE).format(this).toString()
        } catch (e: Exception) {
            null
        }
    }

    /**
     * @see [format(Long, String)]
     */
    fun format(date: Long, format: FormatApi) = format(date, format.pattern)

    /**
     * @see [format(Long, String)]
     */
    fun format(date: Long, format: FormatHumanReadable) = format(date, format.pattern)

    /**
     * Transform the long [date] and parse it to get [String] representation using the given [format]
     * @param date date to format
     * @param format format to use to parse
     */
    fun format(date: Long, format: String) = Date(date).toDateString(format)

    /**
     * Format a given [date] to get a [Date] using the given [format]
     * @param date date to format
     * @param format format to use
     * @return Formatted date, or null
     */
    fun format(date: Date?, format: FormatApi) = getDateFromString(date.toDateString(format.pattern), format.pattern)

    /**
     * Get today's date
     * @return Today
     */
    fun getToday(): Date {
        val calendar = Calendar.getInstance()
        return calendar.time
    }

    /**
     * Get yesterday's date
     * @return Yesterday
     */
    fun getYesterday(): Date {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.DAY_OF_YEAR, -1)
        return calendar.time
    }

    /**
     * Get the first day of the current week
     * @return the first day of the current week
     */
    fun getFirstDayOfCurrentWeek(): Date {
        val calendar = Calendar.getInstance()
        // Calendar.DAY_OF_WEEK 2 = Monday
        calendar.set(Calendar.DAY_OF_WEEK, 2)
        return calendar.time
    }

    /**
     * Get the first day of last week
     * @return the first day of last week
     */
    fun getFirstDayOfLastWeek(): Date {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.WEEK_OF_YEAR, -1)
        // Calendar.DAY_OF_WEEK 2 = Monday
        calendar.set(Calendar.DAY_OF_WEEK, 2)
        return calendar.time
    }

    /**
     * Get the first day of the current month
     * @return the first day of the current month
     */
    fun getFirstDayOfCurrentMonth(): Date {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.DAY_OF_MONTH, 1)
        return calendar.time
    }

    /**
     * Get the last day of the current month
     * @return the last day of the current month
     */
    fun getLastDayOfCurrentMonth(): Date {
        val calendar = Calendar.getInstance()
        val lastDate = calendar.getActualMaximum(Calendar.DATE)
        calendar.set(Calendar.DATE, lastDate)
        return calendar.time
    }

    /**
     * Get the first day of last month
     * @return the first day of last month
     */
    fun getFirstDayOfLastMonth(): Date {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.MONTH, -1)
        calendar.set(Calendar.DAY_OF_MONTH, 1)
        return calendar.time
    }

    /**
     * Get the last day of next month
     * @return the last day of next month
     */
    fun getLastDayOfNextMonth(): Date {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.MONTH, 1)
        val lastDate = calendar.getActualMaximum(Calendar.DATE)
        calendar.set(Calendar.DATE, lastDate)
        return calendar.time
    }

    /**
     * return the month label of the [date]
     * @param date the date
     * @return the label of the month
     */
    fun getMonthLabel(date: Date): String? {
        val calendar = Calendar.getInstance()
        calendar.time = date

        return SimpleDateFormat(FormatHumanReadable.LETTERS_ONLY_MONTH.pattern, Locale.FRANCE).format(date)
    }
}