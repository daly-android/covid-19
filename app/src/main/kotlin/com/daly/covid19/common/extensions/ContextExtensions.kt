package com.daly.covid19.common.extensions

import android.content.Context
import android.widget.Toast
import androidx.annotation.StringRes

/**
 * Context extension to show toast with only resId parameter but for a duration fixed to Toast.LENGTH_LONG
 * @param resId The string resource to show
 */
fun Context.longToast(@StringRes resId: Int) {
    Toast.makeText(this, resId, Toast.LENGTH_LONG).show()
}

/**
 * Context extension to show toast with only text parameter but for a duration fixed to Toast.LENGTH_LONG
 * @param text The string text to show
 */
fun Context.longToast(text: CharSequence) {
    Toast.makeText(this, text, Toast.LENGTH_LONG).show()
}

/**
 * Context extension to show toast with only resId parameter but for a duration fixed to Toast.LENGTH_SHORT
 * @param resId The string resource to show
 */
fun Context.shortToast(@StringRes resId: Int) {
    Toast.makeText(this, resId, Toast.LENGTH_SHORT).show()
}

/**
 * Context extension to show toast with only text parameter but for a duration fixed to Toast.LENGTH_SHORT
 * @param text The string text to show
 */
fun Context.shortToast(text: CharSequence) {
    Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
}