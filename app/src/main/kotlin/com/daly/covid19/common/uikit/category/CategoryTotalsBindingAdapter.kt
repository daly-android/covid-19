package com.daly.covid19.common.uikit.category

import androidx.annotation.ColorInt
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter

/**
 * [CategoryTotalsBindingAdapter] binding adapter for [CategoryLatestTotalsView]
 */
object CategoryTotalsBindingAdapter {

    /**
     * Set a specific tint to the icon category
     */
    @JvmStatic
    @BindingAdapter("image_view_tint")
    fun setImageTintList(imageView: AppCompatImageView, @ColorInt color: Int) {
        imageView.imageTintList = androidx.databinding.adapters.Converters.convertColorToColorStateList(color)
    }

//    @JvmStatic
//    @BindingAdapter("image_view_tint")
//    fun AppCompatImageView.setImageTintList(@ColorRes color: Int) {
//        if (color == 0) return
//        imageTintList = androidx.databinding.adapters.Converters.convertColorToColorStateList(color)
//    }
}