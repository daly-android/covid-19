package com.daly.covid19.common.utils

/**
 * Utility to Category.
 */
class CategoryUtils {

    companion object {

        private const val COMMA = ","
        private const val DOT = "."
        private const val SPACE_REGEX = "\\s"
        private const val SPACES_BETWEEN_NUMBERS_REGEX = "\\B(?=(\\d{3})+(?!\\d))"

        /**
         * Insert spaces between numbers
         * ex : 1000000 -> 1 000 000
         *
         * @param cases
         * @return cases number formatted
         */
        fun insertSpacesBetweenNumbers(cases: String): String = cases.replace(Regex(SPACES_BETWEEN_NUMBERS_REGEX), " ")
    }
}