package com.daly.covid19.common.uikit

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner
import com.daly.covid19.BR

/**
 * Extending this view provides an easy way to create a custom view that act as a Controller for
 * DataBinding layouts.
 * The child class has to provide [layoutId] representing a valid liveData resource layout.
 *
 * With [lifecycle] setter, lifecycleOwner can be passed as parameter inside the xml layout
 * by doing this :
 *
 * ```
 *  <data>
 *      <variable name="lifecycle"
 *          type="androidx.lifecycle.LifecycleOwner" />
 *  </data>
 *  <com.example.MyCustomViewExtendingBaseView
 *      app:lifecycle="@{lifecycle}" />
 * ```
 *
 * Which simplify the need to pass the correct lifecycleOwner in code.
 *
 * Since each databinding layout is unique and can posses multiple variables, BaseView DO NOT
 * handle viewModels getter and setter. Child view must be doing something like :
 * ```
 *  var viewModel: MyViewModel?
 *      get() = binding.viewModel
 *      set(value) {
 *          binding.viewModel = value
 *      }
 * ```
 * and then in XML :
 * ```
 *  <com.example.MyCustomViewExtendingBaseView
 *      app:viewModel="@{variable.that.contain.correct.viewModel}" />
 * ```
 *
 * If the extended class need to do things manually, the correct way is to listen for the lifecycle
 * as well as the viewModel. Child view can listen for lifecycleOwner Setter by listening to [onLifeCycleOwnerSet]
 * */
abstract class BaseView<VB : ViewDataBinding> @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : FrameLayout(context, attrs, defStyleAttr, defStyleRes) {

    /** The lifecycleOwner of the view. */
    open var lifecycle: LifecycleOwner?
        get() = binding.lifecycleOwner
        set(value) {
            binding.lifecycleOwner = value
            binding.setVariable(BR.lifecycle, lifecycle)
            value?.let(this::onLifeCycleOwnerSet)
        }

    /**
     * The ViewDataBinding used by the view.
     * Since it use [layoutId] which is abstract at init time, it raises a Warning.
     * */
    lateinit var binding: VB

    /** For customView who want to get a notification when lifecycleOwner is set */
    open fun onLifeCycleOwnerSet(lifecycleOwner: LifecycleOwner) = Unit

    /** The resource layout Id used by extended view */
    abstract val layoutId: Int

    init {
        // Fix Android-Studio preview of layouts with custom ui views
        if (!isInEditMode) {
            binding = DataBindingUtil.inflate(LayoutInflater.from(context), layoutId, this, true)
        } else {
            LayoutInflater.from(context).inflate(layoutId, this, true)
        }

        // Disable clipping for child view.
        // A cardView shadow will be clip if those variables are not set to false
        clipToPadding = false
        clipChildren = false
    }
}