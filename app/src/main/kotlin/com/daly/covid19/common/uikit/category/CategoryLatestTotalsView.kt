package com.daly.covid19.common.uikit.category

import android.content.Context
import android.util.AttributeSet
import com.daly.covid19.R
import com.daly.covid19.common.uikit.BaseView
import com.daly.covid19.databinding.ViewTotalsCategoryBinding

/**
 * Component that represent the category latest totals
 */
class CategoryLatestTotalsView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : BaseView<ViewTotalsCategoryBinding>(context, attrs, defStyleAttr, defStyleRes) {

    override val layoutId: Int
        get() = R.layout.view_totals_category

    var viewData: CategoryLatestTotalsViewData?
        get() = binding.viewData
        set(value) {
            binding.viewData = value
        }
}