package com.daly.covid19.common.monitoring

object IMonitor {

    abstract class LoggingAction {
        /**
         * Print the message in parameter in the logcat in verbose mode
         * @param message the messaged printed
         */
        abstract fun logV(message: String)

        /**
         * Print the message in parameter in the logcat in debug mode
         * @param message the messaged printed
         */
        abstract fun logD(message: String)

        /**
         * Print the message in parameter in the logcat in warning mode
         * @param message the messaged printed
         */
        abstract fun logW(message: String)

        /**
         * Print the message and throwable in parameter in the logcat in error mode
         * @param message the messaged printed
         * @param throwable the Throwable printed
         */
        abstract fun logE(message: String, throwable: Throwable? = null)
    }
}