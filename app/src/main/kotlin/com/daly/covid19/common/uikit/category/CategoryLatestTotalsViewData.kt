package com.daly.covid19.common.uikit.category

import android.graphics.drawable.Drawable
import androidx.lifecycle.MutableLiveData
import com.daly.covid19.common.uikit.ClickableViewData

/**
 * ViewData of the [CategoryLatestTotalsView] component
 */
class CategoryLatestTotalsViewData : ClickableViewData.Implementation() {
    val categoryIconSrc = MutableLiveData<Drawable>()
    val categoryIconBackground = MutableLiveData<Drawable>()
    val categoryIconTint = MutableLiveData<Int>()
    val categoryTitle = MutableLiveData<String>()
    val categoryValue = MutableLiveData<String>()
}