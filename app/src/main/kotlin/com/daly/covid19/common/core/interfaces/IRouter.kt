package com.daly.covid19.common.core.interfaces

import android.os.Bundle
import androidx.fragment.app.Fragment

/**
 * This interface is used to handle fragments navigation.
 * The initialize method is completely custom
 */
interface IRouter {

    companion object {
        const val FRAGMENT_MANAGER = "IRouter.FragmentManager"
        const val CONTAINER_ID = "IRouter.ContainerID"
    }

    /**
     * Initialize method
     * Depending to the needs of the application, elements are optional and configurable
     * @param arguments Can be anything
     */
    fun init(arguments: Map<String, Any?> = mapOf())

    /**
     * Show a fragment to the navigation stack
     * @param fragment A fragment class that will be instantiate by the system.
     * @param bundle All needed arguments for this fragment.
     * @param isRoot place [fragment] at root’s navigation
     */
    fun <T : Fragment> show(fragment: Class<T>, bundle: Bundle? = null, isRoot: Boolean = false)
}