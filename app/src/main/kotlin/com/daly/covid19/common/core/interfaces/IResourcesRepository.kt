package com.daly.covid19.common.core.interfaces

import android.content.res.TypedArray
import android.graphics.drawable.Drawable
import android.util.TypedValue
import androidx.annotation.ArrayRes
import androidx.annotation.AttrRes
import com.daly.covid19.common.core.implementations.ResourceMustBeOverrideError

/**
 * Domain interface to fetch resources
 * May be injected for multiple purposes (Framework, API, Databases)
 */
interface IResourcesRepository {

    /**
     * Fetch a string according to an id
     * @param id The identifier of the string resources, may be a R.string or database identifier etc.
     * @param args A undefined number of arguments that an implement could use to fetch the data
     */
    fun fetchString(id: Int, vararg args: Any?): String

    /**
     * Fetch a string according to an id. It will throw an [ResourceMustBeOverrideError] if string result is empty
     * @param id The identifier of the string resources, may be a R.string or database identifier etc.
     * @param args A undefined number of arguments that an implement could use to fetch the data
     */
    fun fetchRequiredString(id: Int, vararg args: Any?): String

    /**
     * Fetch a string array according to an id
     * @param id The identifier of the string resources, may be a R.array or database identifier etc.
     * @param args A undefined number of arguments that an implement could use to fetch the data
     */
    fun fetchStringArray(id: Int, vararg args: Any?): Array<out String>

    /**
     * Fetch a string array according to an id. It will throw an [ResourceMustBeOverrideError] if array result is empty
     * @param id The identifier of the string resources, may be a R.array or database identifier etc.
     * @param args A undefined number of arguments that an implement could use to fetch the data
     */
    fun fetchRequiredStringArray(id: Int, vararg args: Any?): Array<out String>

    /**
     * Fetch an array  according to an id
     * @param id The identifier of the resources, may be a R.array or database identifier etc.
     * @param args A undefined number of arguments that an implement could use to fetch the data
     */
    fun fetchTypedArray(@ArrayRes id: Int, vararg args: Any?): TypedArray

    /**
     * Fetch a drawable according to an id
     * @param id The identifier of the drawable resources, may be a R.drawable or database identifier etc.
     * @param args A undefined number of arguments that an implement could use to fetch the data
     */
    fun fetchDrawable(id: Int, vararg args: Any?): Drawable?

    /**
     * Fetch a boolean according to an id
     * @param id The identifier of the boolean resources
     */
    fun fetchBoolean(id: Int): Boolean

    /**
     * Fetch a color according to an id
     * @param id The identifier of the color resources
     */
    fun fetchColor(id: Int): Int

    /**
     * Fetch a quantity string according to an id and a quantity
     * @param id The identifier of the color resources
     * @param quantity The counter to select the correct string to return
     * @param args A undefined number of arguments that an implement could use to fetch the data
     */
    fun fetchQuantityString(id: Int, quantity: Int, vararg args: Any?): String

    /**
     * Fetch a quantity string according to an id and a quantity. It will throw an [ResourceMustBeOverrideError] if string result is empty
     * @param id The identifier of the color resources
     * @param quantity The counter to select the correct string to return
     * @param args A undefined number of arguments that an implement could use to fetch the data
     */
    fun fetchRequiredQuantityString(id: Int, quantity: Int, vararg args: Any?): String

    /**
     * Fetch a resource identifier for the given resource name
     * @param name The name of the desired resource.
     * @param type Optional default resource type to find, if "type/" is
     *                not included in the name.  Can be null to require an
     *                explicit type.
     * @param packageName Optional default package to find, if "package:" is
     *                   not included in the name.  Can be null to require an
     *                   explicit package.
     * @return int The associated resource identifier.  Returns 0 if no such
     *         resource was found.  (0 is not a valid resource ID.)
     */
    fun fetchIdentifier(name: String, type: String, packageName: String): Int

    /**
     * Fetch a integer associated with a particular resource ID.
     * @param id The identifier of the integer resources be a R.integer
     * */
    fun fetchInteger(id: Int): Int

    /**
     * Fetch a dimen associated with a particular resource ID.
     * @param id The identifier of the dimen resources be a R.dimen
     * */
    fun fetchDimen(id: Int): Float

    /**
     * Fetch an attribute resource from the current theme.
     * Attribute resource can be anything. This method fetch any kind of attribute into [typedValue]
     * This method is kinda generic because there is too much possible reference to fetch.
     * color -  dimen - font - string - float - int - colorStateList - array - ...
     *
     * Some common fetcher are defined in this interface like [fetchColorAttribute] or [fetchDrawableAttribute]
     *
     * @param attr attribute to fetch (can be color, dimen, stringRes, font, id, etc.)
     * @param typedValue holder of fetched resource.
     * @param resolveReference it will automatically resolve attr with format="reference"
     *        As example, you can't resolve a font directly into a typedValue you want the reference to that font.
     *        Some references can be resolved directly as int, dimen, color.
     * @return true if the method succeed, false otherwise
     */
    fun resolveThemeAttribute(@AttrRes attr: Int, typedValue: TypedValue, resolveReference: Boolean): Boolean

    /**
     * Resolve a color by attribute
     * @param colorAttr attribute of a color resource
     * @return Color as int.
     * @throws IllegalArgumentException if [colorAttr] can't be resolved
     */
    fun fetchColorAttribute(@AttrRes colorAttr: Int): Int

    /**
     * Resolve a drawable by attribute
     * @param drawableAttr attribute of a drawable resource
     * @return resolved drawable
     * @throws IllegalArgumentException if [drawableAttr] can't be resolved
     */
    fun fetchDrawableAttribute(@AttrRes drawableAttr: Int): Drawable?

    /**
     * Resolve a dimen by attribute
     * @param dimenAttr attribute of a dimen resource
     * @return resolved dimension
     * @throws IllegalArgumentException if [dimenAttr] can't be resolved
     */
    fun fetchDimenAttribute(@AttrRes dimenAttr: Int): Float

    /**
     * Resolve a string by attribute
     * @param stringAttr attribute of a string resource
     * @return resolved string
     * @throws IllegalArgumentException if [stringAttr] can't be resolved
     */
    fun fetchStringAttribute(@AttrRes stringAttr: Int): CharSequence
}