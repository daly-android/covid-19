package com.daly.covid19.common.core.implementations

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit
import com.daly.covid19.common.core.interfaces.IRouter
import com.daly.covid19.common.monitoring.Monitor.logE

/**
 * This interface is used to handle fragments navigation.
 * @see [IRouter]
 */
class Router : IRouter {

    // TODO : Handle show(isRoot)

    private var fragmentManager: FragmentManager? = null
    private var containerId: Int? = null

    /**
     * Exception thrown when initialization failed
     */
    class InitializationException : IllegalArgumentException(
        """
        Map arguments must have :
            - ${IRouter.FRAGMENT_MANAGER} (IRouter.FRAGMENT_MANAGER) a fragment manager
            - ${IRouter.CONTAINER_ID} (IRouter.CONTAINER_ID) a layout id to place fragments inside
        """.trimIndent()
    )

    /**
     * Exception thrown when a method from Rooter is called before init
     */
    class RouterNotInitialized : IllegalStateException("Please call IRouter.init() before any methods")

    /**
     * Called by all methods before executing
     * @return A pair of [FragmentManager] and ContainerId
     */
    @Throws(RouterNotInitialized::class)
    private fun requireInit(): Pair<FragmentManager, Int> =
        (fragmentManager ?: throw RouterNotInitialized()) to (containerId ?: throw RouterNotInitialized())

    /**
     * Implementation of [IRouter.init] to init Router
     * @param arguments the fragment manager and containerId to use for navigation
     */
    @Throws(InitializationException::class)
    override fun init(arguments: Map<String, Any?>) {
        this.fragmentManager = arguments[IRouter.FRAGMENT_MANAGER] as? FragmentManager ?: throw InitializationException()
        this.containerId = arguments[IRouter.CONTAINER_ID] as? Int ?: throw InitializationException()
    }

    /**
     * Implementation of [IRouter.show] to show a fragment to the navigation stack
     * @param fragment A fragment class that will be instantiate by the system.
     * @param bundle All arguments for this fragment.
     * @param isRoot place [fragment] at root’s navigation
     */
    @Throws(RouterNotInitialized::class, IllegalStateException::class, IllegalArgumentException::class)
    override fun <T : Fragment> show(fragment: Class<T>, bundle: Bundle?, isRoot: Boolean) {
        val (fragmentManager, containerId) = requireInit()
        if (fragmentManager.isDestroyed) {
            logE("Cannot show fragment ${fragment.name}: fragmentManager is destroyed")
            return
        }
        fragmentManager.commit {
            replace(containerId, fragment, bundle)
            addToBackStack(fragment.name)
        }
    }
}