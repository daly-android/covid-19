package com.daly.covid19.common.uikit

import android.os.Looper
import androidx.lifecycle.MutableLiveData

/**
 * Generic ViewData handler for clickable element.
 */
interface ClickableViewData {
    val enabled: MutableLiveData<Boolean>
    val clickable: MutableLiveData<Boolean>
    var onClick: (() -> Unit)?
    fun performClick() = onClick?.invoke()

    companion object {
        private const val CLICK_INTERVAL = 500
        private const val ANDROID_OS_LOOPER = "android.os.Looper"
    }

    /** Default implementation of [ClickableViewData] */
    open class Implementation : ClickableViewData {
        override val enabled = MutableLiveData(true)
        override val clickable = MutableLiveData(false)
        override var onClick: (() -> Unit)? = null
            set(value) {
                field = value
                if (isMainThread()) clickable.value = value != null
                else clickable.postValue(value != null)
            }

        private fun isMainThread() = try {
            Class.forName(ANDROID_OS_LOOPER)
            Looper.getMainLooper().isCurrentThread
        } catch (e: Exception) {
            true
        }

        private var lastClickTime: Long = 0
        override fun performClick() {
            if (System.currentTimeMillis() - lastClickTime < CLICK_INTERVAL) return
            lastClickTime = System.currentTimeMillis()
            super.performClick()
        }
    }
}