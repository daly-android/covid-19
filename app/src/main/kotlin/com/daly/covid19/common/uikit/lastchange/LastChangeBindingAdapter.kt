package com.daly.covid19.common.uikit.lastchange

import androidx.appcompat.widget.AppCompatTextView
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import com.daly.covid19.R
import com.daly.covid19.common.utils.DateFormatUtils

/**
 * [LastChangeBindingAdapter] binding adapter for [LastChangeView]
 */
object LastChangeBindingAdapter {

    private const val REGEX_FULL_ISO_8601_TIME_ZONE = "^([\\+-]?\\d{4}(?!\\d{2}\\b))((-?)((0[1-9]|1[0-2])(\\3([12]\\d|0[1-9]|3[01]))?|W([0-4]\\d|5[0-2])(-?[1-7])?|(00[1-9]|0[1-9]\\d|[12]\\d{2}|3([0-5]\\d|6[1-6])))([T\\s]((([01]\\d|2[0-3])((:?)[0-5]\\d)?|24\\:?00)([\\.,]\\d+(?!:))?)?(\\17[0-5]\\d([\\.,]\\d+)?)?([zZ]|([\\+-])([01]\\d|2[0-3]):?([0-5]\\d)?)?)?)?\$" // ktlint-disable max-line-length

    @JvmStatic
    @BindingAdapter("last_change_text")
    fun showLastChange(textView: AppCompatTextView, lastUpdate: String?) {
        if (lastUpdate.isNullOrEmpty() || !lastUpdate.matches(REGEX_FULL_ISO_8601_TIME_ZONE.toRegex())) {
            textView.isVisible = false
        } else {
            val lastUpdateMinutes = DateFormatUtils.getPassedMinutesFromDateMillis(lastUpdate)
            if (lastUpdateMinutes == null) {
                textView.isVisible = false
            } else {
                textView.isVisible = true
                val pluralsFormat = textView.context.resources.getQuantityString(R.plurals.last_change_text, lastUpdateMinutes)
                textView.text = String.format(pluralsFormat, lastUpdateMinutes)
            }
        }
    }
}