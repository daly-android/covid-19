package com.daly.covid19.common.monitoring

import android.annotation.SuppressLint
import android.util.Log
import com.daly.covid19.BuildConfig
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.firebase.crashlytics.ktx.setCustomKeys
import org.koin.core.KoinComponent
import org.koin.core.get
import timber.log.Timber

/**
 * Custom COVID-19 tree in debug to gather the right information of the caller class (position 7 in the tree)
 * MyDebugTree extends from Timber.DebugTree() so no crash reported on Firebase when the build type is Debug
 */
open class MyDebugTree : Timber.DebugTree() {

    companion object {
        const val STACKTRACE_TAG_POSITION = 7
    }

    override fun createStackElementTag(element: StackTraceElement): String? {
        return MonitorImpl.TAG + " - line: " + element.lineNumber + " - " + super.createStackElementTag(Throwable().stackTrace[STACKTRACE_TAG_POSITION])
    }
}

/**
 * Custom COVID-19 tree in release
 *  MyReleaseTree extends from Timber.Tree() so all crashes are reported on Firebase when the build type is Release
 */
open class MyReleaseTree : Timber.Tree() {

    companion object {
        const val CRASHLYTICS_KEY_PRIORITY = "priority"
        const val CRASHLYTICS_KEY_TAG = "tag"
        const val CRASHLYTICS_KEY_MESSAGE = "message"
    }

    public override fun isLoggable(tag: String?, priority: Int): Boolean {
        /**
         * No Log in VERBOSE DEBUG INFO if monitoring is build in RELEASE config
         */
        if (priority == Log.VERBOSE || priority == Log.DEBUG || priority == Log.INFO) {
            return false
        }

        return super.isLoggable(tag, priority)
    }

    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        if (isLoggable(tag, priority)) {
            if (priority == Log.ASSERT) {
                Timber.tag(MonitorImpl.TAG).wtf(tag, message)
            } else if (priority == Log.ERROR || priority == Log.WARN) {
                Log.println(priority, MonitorImpl.TAG + " " + tag, message)
            }

            val crashlytics = FirebaseCrashlytics.getInstance()
            crashlytics.setCustomKeys {
                key(CRASHLYTICS_KEY_PRIORITY, priority)

                // FIXME : Tag is null in Release log
                if (tag != null) {
                    key(CRASHLYTICS_KEY_TAG, tag)
                }
                key(CRASHLYTICS_KEY_MESSAGE, message)
            }

            // FIXME : Replace this testing UserId
            crashlytics.setUserId("12345")

            // FIXME : non-fatal exceptions are not uploaded on Firebase
//            if (t != null) {
//                crashlytics.recordException(t)
//            }

            // Use the recordException method to record non-fatal exceptions in the catch blocks. For example:
//            try {
//                methodThatThrows()
//            } catch (e: Exception) {
//                FirebaseCrashlytics.getInstance().recordException(e)
//                // ...handle the exception.
//            }
        }
    }
}

/**
 * IMonitorTechnical Implementation that use timber.log
 */
class MonitorImpl : IMonitor.LoggingAction() {

    companion object {
        const val TAG = "COVID-19"
    }

    init {
        /**
         * Always plant the tree for error purpose
         */
        Timber.tag(TAG)
        if (BuildConfig.DEBUG) {
            Timber.plant(MyDebugTree())
        } else {
            Timber.plant(MyReleaseTree())
        }
    }

    @SuppressLint("WrongLogger")
    override fun logV(message: String) {
        Timber.v(message)
    }

    @SuppressLint("WrongLogger")
    override fun logD(message: String) {
        Timber.d(message)
    }

    @SuppressLint("WrongLogger")
    override fun logW(message: String) {
        Timber.w(message)
    }

    @SuppressLint("WrongLogger")
    override fun logE(message: String, throwable: Throwable?) {
        Timber.e(throwable, message)
    }
}

object Monitor : KoinComponent, IMonitor.LoggingAction() {

    private val monitor get() = get<MonitorImpl>()

    override fun logV(message: String) {
        monitor.logV(message)
    }

    override fun logD(message: String) {
        monitor.logD(message)
    }

    override fun logW(message: String) {
        monitor.logW(message)
    }

    override fun logE(message: String, throwable: Throwable?) {
        monitor.logE(message, throwable)
    }
}