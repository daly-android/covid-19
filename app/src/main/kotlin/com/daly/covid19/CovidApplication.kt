package com.daly.covid19

import android.app.Application
import com.daly.covid19.common.KoinModuleCommon
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class CovidApplication : Application() {

    companion object {
        init {
            /**
             * Load C library that contains our keys
             */
            System.loadLibrary("keys")
        }
    }

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@CovidApplication)
            modules(MainKoinModule.modules)
            modules(KoinModuleCommon.modules)
        }
    }
}