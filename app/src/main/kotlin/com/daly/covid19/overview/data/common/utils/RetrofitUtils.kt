package com.daly.covid19.overview.data.common.utils

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.daly.covid19.BuildConfig
import com.daly.covid19.R
import com.daly.covid19.common.mock.MockedInterceptor
import com.daly.covid19.overview.data.common.interceptors.AuthenticationInterceptor
import com.daly.covid19.overview.data.common.interceptors.OkHttpLoggerInterceptor
import com.squareup.moshi.Moshi
import java.util.concurrent.TimeUnit
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object RetrofitUtils {

    private const val TIMEOUT = 15L

    /**
     * Provide the retrofit client
     * @param client Okhttp client
     * @param moshi parser json
     * @param url The endpoint
     * @return The retrofit client
     */
    fun provideRetrofitClient(client: OkHttpClient, moshi: Moshi, url: String) =
        Retrofit.Builder()
            .baseUrl(url)
            .client(client)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()

    fun provideOkHttpClientBuilder(): OkHttpClient.Builder =
        OkHttpClient().newBuilder()
            .addInterceptor(
                HttpLoggingInterceptor(logger = OkHttpLoggerInterceptor()).apply {
                    level = HttpLoggingInterceptor.Level.BODY
                }
            )
            .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(TIMEOUT, TimeUnit.SECONDS)

    /**
     * Provide retrofit with API KEY
     * @param context to fetch base_url string resource
     * @param moshi parser json
     * @param authenticationInterceptor IAuthenticationInterceptor
     * @return The API built by a retrofit client
     */
    inline fun <reified T> provideAuthenticateRetrofit(
        context: Context,
        moshi: Moshi,
        authenticationInterceptor: AuthenticationInterceptor
    ): T {

        with(context) {
            val isMockEnabled = resources.getBoolean(R.bool.is_mock_enabled)
            val okHttpClientBuilder = provideOkHttpClientBuilder()
            if (BuildConfig.DEBUG && isMockEnabled) {
                // FIXME : add mock Interceptor is KO
                okHttpClientBuilder.addInterceptor(MockedInterceptor(T::class.java, this))
            } else {
                okHttpClientBuilder.addInterceptor(authenticationInterceptor)
            }
            if (BuildConfig.DEBUG) {
                okHttpClientBuilder.addInterceptor(ChuckerInterceptor.Builder(this).build())
            }
            return provideRetrofitClient(okHttpClientBuilder.build(), moshi, getString(R.string.covid_data_base_url)).create(T::class.java)
        }
    }
}