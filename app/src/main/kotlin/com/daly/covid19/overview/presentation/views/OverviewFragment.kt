package com.daly.covid19.overview.presentation.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.daly.covid19.R
import com.daly.covid19.common.extensions.shortToast
import com.daly.covid19.databinding.FragmentOverviewBinding
import com.daly.covid19.overview.presentation.viewmodels.OverviewFragmentViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class OverviewFragment : Fragment() {

    private lateinit var binding: FragmentOverviewBinding
    private val viewModel: OverviewFragmentViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_overview, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        viewModel.showToast = { context?.shortToast(it) }

        binding.swipeRefresh.setOnRefreshListener {
            // Disable SwipeRefresh default loader because the ViewModel has its own loader
            binding.swipeRefresh.isRefreshing = false
            viewModel.load()
        }

        return binding.root
    }

    override fun onResume() {
        super.onResume()
        viewModel.load()
    }
}