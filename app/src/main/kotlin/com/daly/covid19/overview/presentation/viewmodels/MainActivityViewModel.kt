package com.daly.covid19.overview.presentation.viewmodels

import android.content.Context
import androidx.lifecycle.ViewModel
import com.daly.covid19.AppCoordinator
import com.daly.covid19.common.monitoring.Monitor.logD
import com.daly.covid19.common.monitoring.Monitor.logE
import com.daly.covid19.common.utils.NetworkUtils

class MainActivityViewModel(private val coordinator: AppCoordinator) : ViewModel() {

    var isReady = false

    /**
     * Start OverviewFragment
     */
    fun load(context: Context) {
        logD("Load MainActivityViewModel : Start OverviewFragment")

        if (NetworkUtils.isNetworkAvailable(context)) {
            coordinator.gotToOverview()
        } else {
            logE("No Internet Connection")
            // FIXME : goToNetworkUnavailable() - Create NetworkUnavailableFragment
            coordinator.showNetworkUnavailableAlertDialog(context)
        }.also {
            isReady = true
        }
    }
}