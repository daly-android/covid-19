package com.daly.covid19.overview.data.common.interceptors

import com.daly.covid19.common.monitoring.IMonitor
import com.daly.covid19.common.monitoring.Monitor.logV
import com.daly.covid19.common.monitoring.MyDebugTree
import com.daly.covid19.common.monitoring.MyReleaseTree
import okhttp3.logging.HttpLoggingInterceptor

/**
 * OkHttp logger that simply redirect to our main logger implementation of [IMonitor]
 * All messages are logged as verbose
 * Verbose logging is just enabled for Debug build type : @see [MyDebugTree] and [MyReleaseTree]
 * If build type is Debug we log OkHttp requests/responses
 * If build type is Release we do not log OkHttp requests/responses
 */
class OkHttpLoggerInterceptor : HttpLoggingInterceptor.Logger {
    override fun log(message: String) {
        logV(message)
    }
}