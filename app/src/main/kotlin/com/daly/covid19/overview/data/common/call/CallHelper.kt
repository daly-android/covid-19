package com.daly.covid19.overview.data.common.call

import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.single
import okhttp3.Headers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

// suspend fun <T> safeApiCall(apiCall: suspend () -> T): ApiResult<T> {
//    return withContext(Dispatchers.IO) {
//        try {
//            ApiResult.Success(apiCall.invoke())
//        } catch (throwable: Throwable) {
//            when (throwable) {
//                is IOException -> ApiResult.Error(ApiError.NetworkError)
//                is HttpException -> {
//                    val errorCode = throwable.code()
//                    val errorMessage = throwable.message()
//                    // To get errorBody we need to create a data class to represent ErrorResponse and make its attributes as serializable using mush or Gson
//                    ApiResult.Error(ApiError.TechnicalError(errorCode, errorMessage, throwable))
//                }
//                // TODO : Handle FunctionalError
//                else -> {
//                    ApiResult.Error(ApiError.GenericError(null, null))
//                }
//            }
//        }
//    }
// }

/**
 * Extension for the class [Call] that can handle a request with successCallback and failureCallback
 * @param onFailureCallback function called when request is failed. Take in parameter an [ApiResult.Error] object.
 * @param onSuccessCallback function called when request is succeed. Take in parameter an [ApiResult.Success] object.
 */
fun <T : Any> Call<T>.callApi(
    onFailureCallback: (error: ApiResult.Error) -> Unit,
    onSuccessCallback: (success: ApiResult.Success<T>) -> Unit
) {
    this.enqueue(object : Callback<T> {
        override fun onFailure(call: Call<T>, t: Throwable) {
            onFailureCallback(
                ApiResult.Error(
                    ApiError.Internal(Headers.Builder().build(), "Internal error occurred. Maybe there is no internet connection.", t)
                )
            )
        }

        override fun onResponse(call: Call<T>, response: Response<T>) {
            if (response.isSuccessful) {
                onSuccessCallback(ApiResult.Success(response.headers(), response.body(), response.code()))
                // response is automatically closed
            } else {
                onFailureCallback(ApiResult.Error(ApiError.Failure(response.headers(), response.errorBody()?.string(), response.message(), response.code())))
                response.closeFailureResponse()
            }
        }
    })
}

private fun Response<*>.closeFailureResponse() = errorBody()?.close()

/**
 * Extension for the class [Call] that can handle a request and return a coroutine.flow
 * @return coroutine.flow with an [ApiResult.Error] object or an [ApiResult.Success] object.
 */
fun <T : Any> Call<T>.flowApi() = flow<ApiResult<T>> {
    // without buffer, in a environment with only one thread, the call can offer() before the receive().
    // effectively missing some outputs.
    val result: Channel<ApiResult<T>> = Channel(Channel.BUFFERED)

    this@flowApi.callApi(
        { result.trySend(it) },
        { result.trySend(it) }
    )
    emit(result.receive())
}

/**
 * Extension for the class [Call] that can handle a request and return the ApiResult.Success or null if error
 * This extension function can be use with flow’s catch function.
 * @return An [ApiResult.Success]
 */
suspend fun <T : Any> Call<T>.quickCallApi() = this.flowApi().single()