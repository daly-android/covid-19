package com.daly.covid19.overview.presentation.mappers

import com.daly.covid19.R
import com.daly.covid19.common.core.interfaces.IResourcesRepository
import com.daly.covid19.common.uikit.category.CategoryLatestTotalsViewData
import com.daly.covid19.common.uikit.lastchange.LastChangeViewData
import com.daly.covid19.common.utils.CategoryUtils
import com.daly.covid19.overview.domain.models.LatestTotalsModel
import com.daly.covid19.overview.domain.models.LatestTotalsModel.Category
import com.daly.covid19.overview.domain.models.LatestTotalsModel.CategoryInfo

fun LatestTotalsModel.toViewData(
    resourcesRepository: IResourcesRepository
): Pair<List<CategoryLatestTotalsViewData>, LastChangeViewData> {
    val latestTotalsViewData = arrayListOf<CategoryLatestTotalsViewData>()
    for (categoryInfo in this@toViewData.categories) {
        latestTotalsViewData.add(setupCategoryLatestTotalsViewData(categoryInfo, resourcesRepository))
    }

    val lastChangeViewData = LastChangeViewData().apply {
        lastChangeText.postValue(this@toViewData.lastChange)
    }

    return Pair(latestTotalsViewData, lastChangeViewData)
}

fun setupCategoryLatestTotalsViewData(categoryInfo: CategoryInfo, resourcesRepository: IResourcesRepository) = when (categoryInfo.category) {
    Category.CONFIRMED -> {
        CategoryLatestTotalsViewData().apply {
            categoryIconSrc.postValue(resourcesRepository.fetchDrawable(R.drawable.ic_confirmed))
            categoryIconBackground.postValue(resourcesRepository.fetchDrawable(R.drawable.background_confirmed_icon))
            categoryIconTint.postValue(resourcesRepository.fetchColor(R.color.white))
            categoryTitle.postValue(resourcesRepository.fetchString(R.string.confirmed))
        }
    }
    Category.RECOVERED -> {
        CategoryLatestTotalsViewData().apply {
            categoryIconSrc.postValue(resourcesRepository.fetchDrawable(R.drawable.ic_recovered))
            categoryIconBackground.postValue(resourcesRepository.fetchDrawable(R.drawable.background_recovered_icon))
            categoryIconTint.postValue(resourcesRepository.fetchColor(R.color.white))
            categoryTitle.postValue(resourcesRepository.fetchString(R.string.recovered))
        }
    }
    Category.CRITICAL -> {
        CategoryLatestTotalsViewData().apply {
            categoryIconSrc.postValue(resourcesRepository.fetchDrawable(R.drawable.ic_critical))
            categoryIconBackground.postValue(resourcesRepository.fetchDrawable(R.drawable.background_critical_icon))
            categoryIconTint.postValue(resourcesRepository.fetchColor(R.color.black))
            categoryTitle.postValue(resourcesRepository.fetchString(R.string.critical))
        }
    }
    Category.DEATHS -> {
        CategoryLatestTotalsViewData().apply {
            categoryIconSrc.postValue(resourcesRepository.fetchDrawable(R.drawable.ic_deaths))
            categoryIconBackground.postValue(resourcesRepository.fetchDrawable(R.drawable.background_deaths_icon))
            categoryIconTint.postValue(resourcesRepository.fetchColor(R.color.black))
            categoryTitle.postValue(resourcesRepository.fetchString(R.string.deaths))
        }
    }
}.also {
    it.categoryValue.postValue(CategoryUtils.insertSpacesBetweenNumbers(categoryInfo.value.toString()))
}