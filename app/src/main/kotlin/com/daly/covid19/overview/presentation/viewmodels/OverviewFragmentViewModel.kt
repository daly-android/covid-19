package com.daly.covid19.overview.presentation.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.daly.covid19.R
import com.daly.covid19.common.core.interfaces.IResourcesRepository
import com.daly.covid19.common.monitoring.Monitor.logD
import com.daly.covid19.common.uikit.category.CategoryLatestTotalsViewData
import com.daly.covid19.common.uikit.lastchange.LastChangeViewData
import com.daly.covid19.overview.domain.models.LatestTotalsModel
import com.daly.covid19.overview.domain.repositories.IOverviewRepository
import com.daly.covid19.overview.domain.repositories.RepositoryError
import com.daly.covid19.overview.domain.repositories.RepositoryResult
import com.daly.covid19.overview.presentation.mappers.toViewData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.singleOrNull
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * [com.daly.covid19.overview.presentation.views.OverviewFragment] viewModel. It manages all data for latest totals.
 * By default it shows no results until [load] is called
 */
@OptIn(ExperimentalCoroutinesApi::class)
class OverviewFragmentViewModel(
    private val resourcesRepository: IResourcesRepository,
    private val overviewRepository: IOverviewRepository
) : ViewModel() {

    var showToast: ((message: String) -> Unit) = {}

    /** All the latest totals cases. Updated after [load] */
    val latestTotalsCases = MutableLiveData<List<CategoryLatestTotalsViewData>?>()

    // FIXME : categoryIconSrc, categoryIconBackground,... are defined twice : in this viewModel and in the ViewMapper
    val confirmedCases = MutableLiveData<CategoryLatestTotalsViewData?>()
    val recoveredCases = MutableLiveData<CategoryLatestTotalsViewData>()
    val criticalCases = MutableLiveData<CategoryLatestTotalsViewData>()
    val deathsCases = MutableLiveData<CategoryLatestTotalsViewData>()

    val lastChange = LastChangeViewData()

    init {
        confirmedCases.value = CategoryLatestTotalsViewData().apply {
            categoryIconSrc.value = resourcesRepository.fetchDrawable(R.drawable.ic_confirmed)
            categoryIconBackground.value = resourcesRepository.fetchDrawable(R.drawable.background_confirmed_icon)
            categoryIconTint.value = resourcesRepository.fetchColor(R.color.white)
            categoryTitle.value = resourcesRepository.fetchString(R.string.confirmed)
            categoryValue.value = resourcesRepository.fetchString(R.string.unavailable)
            onClick = { showToast("Confirmed cases clicked !") }
        }

        recoveredCases.value = CategoryLatestTotalsViewData().apply {
            categoryIconSrc.value = resourcesRepository.fetchDrawable(R.drawable.ic_recovered)
            categoryIconBackground.value = resourcesRepository.fetchDrawable(R.drawable.background_recovered_icon)
            categoryIconTint.value = resourcesRepository.fetchColor(R.color.white)
            categoryTitle.value = resourcesRepository.fetchString(R.string.recovered)
            categoryValue.value = resourcesRepository.fetchString(R.string.unavailable)
        }

        criticalCases.value = CategoryLatestTotalsViewData().apply {
            categoryIconSrc.value = resourcesRepository.fetchDrawable(R.drawable.ic_critical)
            categoryIconBackground.value = resourcesRepository.fetchDrawable(R.drawable.background_critical_icon)
            categoryIconTint.value = resourcesRepository.fetchColor(R.color.black)
            categoryTitle.value = resourcesRepository.fetchString(R.string.critical)
            categoryValue.value = resourcesRepository.fetchString(R.string.unavailable)
        }

        deathsCases.value = CategoryLatestTotalsViewData().apply {
            categoryIconSrc.value = resourcesRepository.fetchDrawable(R.drawable.ic_deaths)
            categoryIconBackground.value = resourcesRepository.fetchDrawable(R.drawable.background_deaths_icon)
            categoryIconTint.value = resourcesRepository.fetchColor(R.color.black)
            categoryTitle.value = resourcesRepository.fetchString(R.string.deaths)
            categoryValue.value = resourcesRepository.fetchString(R.string.unavailable)
        }
    }

    /**
     * Preload latest totals and render them on the screen
     */
    fun load() = viewModelScope.launch(Dispatchers.Default) {
        logD("load latest cases ...")
        loading { getLatestCases() }
    }

    /**
     * Get cards by calling the [IOverviewRepository]
     * It transforms [LatestTotals] into [LatestTotalsModel]
     */
    private suspend fun getLatestCases() = withContext(Dispatchers.IO) {
        when (val response = overviewRepository.fetchLatestTotals().singleOrNull()) {
            is RepositoryResult.Success<*> -> {
                latestTotalsCases.postValue((response.data as LatestTotalsModel).toViewData(resourcesRepository).first)
                lastChange.apply {
                    lastChangeText.postValue(response.data.toViewData(resourcesRepository).second.lastChangeText.value)
                }
            }
            is RepositoryResult.Failure -> {
                when (response.error) {
                    RepositoryError.Internal -> handleInternalError()
                    RepositoryError.NotAuthorized -> handleNotAuthorizedError()
                    RepositoryError.GenericFailure -> handleGenericError()
                }
            }
            null -> handleGenericError()
        }
        updateCategories()
    }

    private suspend fun updateCategories() = withContext(Dispatchers.Main) {
        latestTotalsCases.value?.let {
            confirmedCases.value = it[0]
            recoveredCases.value = it[1]
            criticalCases.value = it[2]
            deathsCases.value = it[3]
        }
    }

    private suspend fun handleNotAuthorizedError() = withContext(Dispatchers.Main) {
        showToast("You are not authorized to load latest cases")
    }

    private suspend fun handleInternalError() = withContext(Dispatchers.Main) {
        showToast("A network problem occurred when loading latest cases")
    }

    private suspend fun handleGenericError() = withContext(Dispatchers.Main) {
        showToast("A problem occurred when loading latest cases")
    }

    /** Progress indicator */
    val inProgress = MutableLiveData(false)

    /** show a progress on screen during action runtime */
    private inline fun <T> loading(action: () -> T): T {
        inProgress.postValue(true)
        val result = action()
        inProgress.postValue(false)
        return result
    }
}