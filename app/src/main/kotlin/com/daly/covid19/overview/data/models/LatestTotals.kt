package com.daly.covid19.overview.data.models

import com.squareup.moshi.Json

/**
 *
 * @param confirmed
 * @param recovered
 * @param critical
 * @param critical
 * @param deaths
 * @param lastChange
 * @param lastUpdate
 */
data class LatestTotals(
    @Json(name = "confirmed")
    val confirmed: Long? = null,

    @Json(name = "recovered")
    val recovered: Long? = null,

    @Json(name = "critical")
    val critical: Long? = null,

    @Json(name = "deaths")
    val deaths: Long? = null,

    @Json(name = "lastChange")
    val lastChange: String? = null,

    @Json(name = "lastUpdate")
    val lastUpdate: String? = null,
)