package com.daly.covid19.overview.data.common.call

import okhttp3.Headers

/**
 * Sealed class for ApiResult
 * @property Success data class created when a request succeed.
 * @property Error data class created when a request failed. It contains an [ApiError] object.
 */
sealed class ApiResult<out T> {
    data class Success<out T>(val headers: Headers, val body: T?, val code: Int?) : ApiResult<T>()
    data class Error(val error: ApiError) : ApiResult<Nothing>()

    /** Utility function to cast success as [ApiResult.Success] */
    fun asSuccess() = this as? Success<T>

    /** Utility function to cast error as [ApiResult.Error] */
    fun asError() = this as? Error
}