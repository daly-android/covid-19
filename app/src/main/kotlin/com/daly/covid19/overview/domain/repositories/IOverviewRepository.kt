package com.daly.covid19.overview.domain.repositories

import kotlinx.coroutines.flow.Flow

/**
 * Domain interface to fetch latest total cases
 * Must be implemented by data layer and use by presentation layer (UseCases)
 */
interface IOverviewRepository {

    /** Flow that collect latest data for whole world or null if API failed or not exist */
    fun fetchLatestTotals(): Flow<RepositoryResult?>
}