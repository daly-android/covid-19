package com.daly.covid19.overview.data.mappers

import com.daly.covid19.overview.data.models.LatestTotals
import com.daly.covid19.overview.domain.models.LatestTotalsModel
import com.daly.covid19.overview.domain.models.LatestTotalsModel.Category
import com.daly.covid19.overview.domain.models.LatestTotalsModel.CategoryInfo

/**
 * Mapper that transform a [LatestTotals] into a [LatestTotalsModel]
 */
fun LatestTotals.toDomainModel() = LatestTotalsModel(
    categories = getCategoriesInfo(this@toDomainModel),
    lastChange = this.lastChange,
    lastUpdate = this.lastUpdate
)

private fun getCategoriesInfo(latestTotals: LatestTotals): List<CategoryInfo> {
    val categories = arrayListOf<CategoryInfo>()
    categories.add(CategoryInfo(Category.CONFIRMED, latestTotals.confirmed ?: 0L))
    categories.add(CategoryInfo(Category.RECOVERED, latestTotals.recovered ?: 0L))
    categories.add(CategoryInfo(Category.CRITICAL, latestTotals.critical ?: 0L))
    categories.add(CategoryInfo(Category.DEATHS, latestTotals.deaths ?: 0L))
    return categories
}