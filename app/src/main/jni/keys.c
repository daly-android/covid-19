/*** Includes **********************************/
#include <jni.h>
#include <android/log.h>

/*** Defines ***********************************/
#define LOG_TAG "JNI"
#define MAX_ARRAY_SIZE 60

/**
 * Get covid-19-data API KEY
 *
 * To avoid being able to see the API KEY by opening the ".so" generated file, we add chars one by one using arrays to complicate the hacker's task
 *
 * @param env Java Native Interface environment
 * @param "this" reference
 * @return the API KEY to use
 */
JNIEXPORT jstring JNICALL
Java_com_daly_covid19_overview_data_common_interceptors_AuthenticationInterceptor_getApiKey(JNIEnv *env, jobject this) {
    __android_log_print(ANDROID_LOG_VERBOSE, LOG_TAG, "%s", __func__);

    char apiKey[MAX_ARRAY_SIZE] = {
            '1', '4', '1', '9', '6', '2', '7', '8',
            '9', 'b', 'm', 's', 'h', '5', 'c', '6',
            '6', '9', '7', 'd', '8', 'a', 'b', '5',
            '0', 'd', 'd', '2', 'p', '1', '2', 'd',
            '3', '2', '5', 'j', 's', 'n', '0', 'c',
            '3', '6', 'f', '0', '3', 'f', '7', '0',
            'e', '9'
    };
    return (*env)->NewStringUTF(env, apiKey);
}