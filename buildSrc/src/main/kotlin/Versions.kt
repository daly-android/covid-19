object Versions {

    object Android {
        const val APP_COMPAT = "1.2.0"
        const val CORE_KTX = "1.3.2"
        const val LIFECYCLE = "2.3.0"
        const val LIFECYCLE_EXTENSIONS = "2.2.0"
        const val FRAGMENT = "1.2.5"
        const val DATABINDING = "3.5.0"
        const val SPLASH_SCREEN = "1.0.0-beta01"
    }

    object Asynchronous {
        const val COROUTINES = "1.4.1"
    }

    object Api {
        const val JSON_MOSHI_KOTLIN = "1.9.2"
        const val JSON_MOSHI_ADAPTER = "1.9.2"
        const val THREETENBP_ADAPTER = "1.4.0"
    }

    object Classpath {
        const val ANDROID_TOOLS_R8 = "3.1.51"
        const val ANDROID_TOOLS_GRADLE = "7.0.4"
        const val JACOCO = "0.8.7"
        const val JUNIT_5 = "1.6.2.0"
        const val GOOGLE_SERVICES = "4.3.5"
        const val FIREBASE_CRASHLYTICS = "2.5.2"
    }

    object DependencyInjection {
        const val KOIN = "2.1.6"
    }

    object Kotlin {
        const val KOTLIN_VERSION = "1.6.10"
    }

    object Lint {
        const val KTLINT = "0.40.0"
    }

    object Logging {
        const val TIMBER = "5.0.1"
    }

    object Monitoring {
        // When using the BoM, you don't specify versions in Firebase library dependencies
        const val FIREBASE_BOM = "27.1.0"
    }

    object Network {
        const val RETROFIT = "2.6.2" // 2.7+ minSdkVersion 26
        const val LOGGING_INTERCEPTOR = "4.9.0"
        const val CHUCKER = "3.5.2"
    }

    object Others {
        const val PODAM = "7.2.6.RELEASE"
        const val GSON = "2.8.6"
    }

    object Plugins {
        const val GRADLE_VERSIONS = "0.42.0"
    }

    object Testing {
        const val CORE_TESTING = "2.1.0"
        const val GOOGLE_TRUTH = "0.44"
        const val JACOCO = "0.8.7"
        const val JUNIT5 = "5.7.0"
        const val MOCKK = "1.10.0"
        const val SONARQUBE = "2.8"

        // Making sure test version is the same as app version.
        const val COROUTINES = Asynchronous.COROUTINES
    }

    object UI {
        const val CONSTRAINT_LAYOUT = "2.0.4"
        const val MATERIAL = "1.3.0"
        const val SWIPE_REFRESH_LAYOUT = "1.1.0"
    }
}